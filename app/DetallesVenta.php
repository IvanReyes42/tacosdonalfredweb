<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallesVenta extends Model
{
    // Relacion uno a muchos
    public function Menu(){
        return $this->hasMany('App\Menu');
    }
    //Relacion uno a muchos inversa
    public function Venta(){
        return $this->belongsTo('App\Venta');
    }
}
