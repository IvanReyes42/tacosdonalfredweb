<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetallesVenta;
use App\Venta;
use  App\Http\Controllers\URL;
use Illuminate\Support\Facades\DB;
use Dompdf\Dompdf;

class CobrarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //return view('CobrarCajero.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //$Detalle = DetallesVenta::where('FkIdVenta',$id)->get();
        $Detalle = DB::table('menus')
            ->join('detalles_ventas','detalles_ventas.FkIdPlatillo','=','menus.IdPlatillo')
            ->select('detalles_ventas.id','detalles_ventas.Cantidad','detalles_ventas.Precio','menus.Nombre')->where('detalles_ventas.FkIdVenta',$id)->get();
        //dd($Detalle);
        $Ventas = Venta::find($id);

        return view('CobrarCajero.index',['Detalles'=>$Detalle,'Venta'=>$Ventas,'IdVenta'=>$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Venta = Venta::find($id);
        $Venta->Estatus = "Entregado";

        if($Venta->save())
        {
            return redirect('Venta');
        }
        else
        {
            return redirect('CobrarCajero.index/'.$id);
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pdf($id)
    {
        $Tabla='';
        $Venta = Venta::find($id);
        $Detalle = DB::table('menus')
            ->join('detalles_ventas','detalles_ventas.FkIdPlatillo','=','menus.IdPlatillo')
            ->select('detalles_ventas.id','detalles_ventas.Cantidad','detalles_ventas.Precio','menus.Nombre')->where('detalles_ventas.FkIdVenta',$id)->get();

        //require 'vendor/autoload.php';
       // include_once "./vendor/autoload.php";
       foreach ($Detalle as $detalle)
       {
           $Tabla .= '
           <tr>
                <th>'.$detalle->Cantidad.'</th>
                <td>'.$detalle->Nombre.' </td>
                <td>$'.$detalle->Precio.'</td>
                <td>$'.$detalle->Cantidad*$detalle->Precio.'</td>
            </tr>
           ';
       }
        $Imagen ="'./Imagenes/LogoImagen.png'";

        $html='
        <html>
        <head>
        <style>
         * {
            font-size: 14px;
            font-family: "Arial", serif;
        }
        td,
        th,
        tr,
        table {
            border-top: 1px solid black;
            border-collapse: collapse;
            margin: 0 auto;
        }
        .ticket {
            width: 250px;
            max-width: 250px;
        }
        h1 {
            font-size: 18px;
        }
        .ticket {
            margin: 2px;
        }
        th {
            text-align: center;
        }
        .centrado {
            text-align: center;
            align-content: center;
        }
        * {
            margin: 0;
            padding: 0;
        }

        .ticket {
            margin: 0;
            padding: 0;
        }
        </style>
        <title>Ticket #'.$Venta->id.'</title>
        </head>
        <body class="ticket centrado">
        <div class="row">

        <div class="col-6 col-md-6 col-lg-6 col-sm-6">
        <h1>Tacos Don Alfred</h1>
        <h1>Ticket #'.$Venta->id.'</h1>
         <h3>Cliente '.$Venta->Mesa.'</h3>

        </div>
        </div>
          <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Cantidad  </th>
                <th scope="col">Producto  </th>
                <th scope="col">Precio Unitario  </th>
                <th scope="col">Subtotal  </th>

            </tr>
            </thead>
            <tbody>
            '.$Tabla.'
            </tbody>
        </table>
        <h2>Su Total es : $'.$Venta->Total.'</h2>
        <p>Gracias por su Compra</p>
        <p>Restaurante de Tacos Don Alfred Ignacio López Rayon 814, Centro, 47400 Lagos de Moreno, Jal.  </p>
        </body>
        </html>
        ';
        $dompdf = new Dompdf();
        $dompdf->setPaper('b7', 'portrait');

        $dompdf->loadHtml($html);
        $dompdf->render();
        $dompdf->stream("Ticket.pdf",array('Attachment'=>'0'));

    }


}
