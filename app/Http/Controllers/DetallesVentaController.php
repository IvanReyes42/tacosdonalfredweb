<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use Illuminate\Support\Facades\DB;
use App\DetallesVenta;
use Illuminate\Support\Facades\View;
use Illuminate\Routing\Route;


class DetallesVentaController extends Controller
{

    public function show($id)
    {
        //
        //config(['global.IdVenta' => $id]);
        //dd(config('global.IdVenta'));
        //$this->x = $id;
        //$_SESSION['x'] = $id;
        session(['x'=>$id]);
        $Articulos = Menu::all();
        //$Detalle = DetallesVenta::where('FkIdVenta',$id)->get();
        $Detalle = DB::table('menus')
            ->join('detalles_ventas','detalles_ventas.FkIdPlatillo','=','menus.IdPlatillo')
            ->select('detalles_ventas.id','detalles_ventas.FkIdPlatillo','detalles_ventas.Cantidad','detalles_ventas.Precio','detalles_ventas.Comentarios','menus.Nombre')->where('detalles_ventas.FkIdVenta',$id)->get();
        //dd($Detalle);

        $Total = $this->CalcularTotal($id);

        return view('DetallesVenta.index',['Articulos'=>$Articulos,'Detalles'=>$Detalle,'Total'=>$Total,'IdVenta'=>$id]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd(config('global.IdVenta'));
        $valorx = session('x');
        $valorx +=0;

        $DetalleVenta = new DetallesVenta();
        $DetalleVenta->FkIdVenta = $valorx;
        $DetalleVenta->FkIdPlatillo = $request->Id;
        $DetalleVenta->Cantidad = 1;
        $DetalleVenta->Comentarios = "";
        $DetalleVenta->Precio = $request->Precio;



        if($this->ValidarProducto($DetalleVenta->FkIdPlatillo,$valorx))
        {
            //Actualizar;
            $this->Actualizar($DetalleVenta->FkIdPlatillo);

            return redirect('DetallesVenta/'.$valorx);
        }
        else
        {
            if($DetalleVenta->save())
            {

                return redirect('DetallesVenta/'.$valorx);
            }
        }
    }
    public function  Actualizar($id)
    {
        $valorx = session('x');
        $valorx +=0;

        $DetalleVentas = DetallesVenta::where('FkIdPlatillo',$id)->where('FkIdVenta',$valorx)->first();

        $DetalleVentas->Cantidad = $DetalleVentas->Cantidad+1;
        if($DetalleVentas->save())
        {
            //$Total = $this->CalcularTotal(1);
            return redirect('DetallesVenta/'.$valorx);
        }
    }
    public function  Actualizarmenos($id)
    {
        $valorx = session('x');
        $valorx +=0;
        $DetalleVentas = DetallesVenta::where('FkIdPlatillo',$id)->where('FkIdVenta',$valorx)->first();
        $DetalleVentas->Cantidad = $DetalleVentas->Cantidad-1;
        if($DetalleVentas->Cantidad >1)
        {
            if($DetalleVentas->save())
            {
                //$Total = $this->CalcularTotal(1);
                return redirect('DetallesVenta/'.$valorx);
            }
        }
        else if($DetalleVentas->Cantidad ==1)
        {
            $this->destroy($DetalleVentas->id);
            return redirect('DetallesVenta/'.$valorx);
        }
    }
    public function ValidarProducto($IdProducto, $IdVenta)
    {
        $Valor = false;
        $Detalles = DetallesVenta::where('FkIdVenta',$IdVenta)->get();

        for ($i = 0;$i< count($Detalles); $i++)
        {

            if($Detalles[$i]->FkIdPlatillo == $IdProducto){
                $Valor = True;
                $i = count($Detalles);
            }
        }
        return $Valor;
    }
    public function CalcularTotal($IdVenta)
    {
        $Detalles = DetallesVenta::where('FkIdVenta',$IdVenta)->get();
        $Total = 0;
        for ($i = 0;$i< count($Detalles); $i++)
        {
            $Total += $Detalles[$i]->Cantidad * $Detalles[$i]->Precio;
        }
        return $Total;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function Comentario($id)
    {
        $valorx = session('x');
        $valorx +=0;

        $Detalles = DetallesVenta::find($id);
        $Detalles->Comentarios = "";

        if($Detalles->save())
        {
            return redirect('DetallesVenta/'.$valorx);
        }
        else
        {
            return redirect('DetallesVenta/'.$valorx);
        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $valorx = session('x');
        $valorx +=0;

        $Detalles = DetallesVenta::find($id);
        $Detalles->Comentarios = $request->Comentario;

        if($Detalles->save())
        {
            return redirect('DetallesVenta/'.$valorx);
        }
        else
        {
            return redirect('DetallesVenta/'.$valorx);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $valorx = session('x');
        $valorx +=0;
        DetallesVenta::destroy($id);
        return redirect('DetallesVenta/'.$valorx);
    }
    public function cantidad(Request $request, $id)
    {
        $valorx = session('x');
        $valorx +=0;
        $Detalle = DetallesVenta::find($id);
        $Detalle->Cantidad = $request->Cantidad;
        if($Detalle->save())
        {
            return redirect('DetallesVenta/'.$valorx);
        }
        else
        {
            return redirect('DetallesVenta/'.$valorx);
        }
    }
}
