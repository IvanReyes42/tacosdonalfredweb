<?php

namespace App\Http\Controllers;

use App\DetallesVenta;
use Illuminate\Http\Request;
use App\Venta;
use App\Empleado;
use auth;

class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Ventas = Venta::whereIn('Estatus',['En Proceso','Por Cobrar'])->get();
        $id = Auth::user()->id;
        $user = Empleado::select('Puesto')->where('FkIdUsuario',$id)->first();

        return view('Venta.index',['Ventas'=>$Ventas,'Usuario'=>$user]);
    }
    public  function Actualizar(Request $request)
    {
        return('Entro a Actualizar');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $hoy = date("Y")."-".date("m")."-".date("d");
        $Venta = new Venta();
        //$Venta->id = null;
        $Venta->Fecha = $hoy;
        if($request->Mesa == "Cliente para Llevar")
        {
            $Venta->Mesa = $request->Cliente;
        }
        else
        {
            $Venta->Mesa = $request->Mesa;
        }
        $Venta->Estatus = "En Proceso";
        $Venta->Total = 0;
        $Venta->FkIdEmpleado = Auth::user()->id;
        if($Venta->save())
        {

            return redirect('/Venta');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Venta = Venta::find($id);
        $Venta->Estatus = "Por Cobrar";
        //dd($Venta);
        if($Venta->save())
        {
            return redirect('Taquero');
        }
        else
        {
            return redirect('Taquero');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $Venta = Venta::where('id',$id)->first();
        $Venta->Total = $request->Total;

        if($Venta->save())
        {
            return redirect('Venta');
        }
        else
        {
            return redirect('Cobrar/'.$id);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return ('Entro a Eliminar');
    }

    public function Cancelar($id)
    {
        $Detalles = DetallesVenta::where('FkIdVenta',$id)->select('id')->get();
        foreach ($Detalles as $Detalle){
            DetallesVenta::destroy($Detalle->id);
        }
        $Venta = Venta::find($id);
        $Venta->Estatus = "Cancelado";
        //dd($Venta);
        if($Venta->save())
        {
            return redirect('/Venta');
        }
        else
        {
            return redirect('/Venta');
        }
    }
}
