<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    //
    public function DetallesVentas()
    {
        return$this->hasMany('App\DetallesVenta');
    }
}
