@extends('layouts.app')

@section('content')


    <div style="background: teal;">
        <div class="container"  >
            <nav class="navbar navbar-expand-lg pt-2 mt-0">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <a class="navbar-brand" href="{{route('home')}}" style="color: white">Inicio</a>
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('CajeroMenu.index')}}" style="color: white">Cajero</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('Venta.index')}}" style="color: white">Mesero</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('Taquero.index')}}" style="color: white">Taquero</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <div class="container-fluid pt-4 pb-4" style="background: teal; color: white">
        <h1 class="text-center">{{$Venta->Mesa}}</h1>
    </div>
    <div class="container pt-4">
        <table class="table">
            <thead style="background: darkorange; color: white">
            <tr>
                <th scope="col">Cantidad</th>
                <th scope="col">Producto</th>
                <th scope="col">Precio Unitario</th>
                <th scope="col">Subtotal</th>

            </tr>
            </thead>
            <tbody>
            @foreach($Detalles as $Detalle)
            <tr>
                <th>{{$Detalle->Cantidad}}</th>
                <td>{{$Detalle->Nombre}} </td>
                <td>$ {{$Detalle->Precio}}</td>
                <td>$ {{$Detalle->Cantidad*$Detalle->Precio}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-12 col-lg-7">
                <div class="row">
                    <div class="col-4"> <a href="{{url('/Cancelar/'.$Venta->id)}}" class="btn btn-danger">Cancelar Pedido</a></div>
                    <div class="col-4"> <a href="{{url('/PDF/'.$Venta->id)}}" class="btn btn-primary" target="_blank">Ticket</a></div>
                    <div class="col-4"> <a href="{{route('Cobrar.edit',$Venta->id)}}" class="btn btn-primary">Cobrar</a></div>
                </div>

            </div>
            <div class="col-12 col-lg-5">
                <h4>Su total es: $ {{$Venta->Total}}</h4>
            </div>
        </div>
    </div>


@endsection
