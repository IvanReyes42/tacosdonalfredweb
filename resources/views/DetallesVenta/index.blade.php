@extends('layouts.app')

@section('content')

    <div style="background: teal;">
        <div class="container"  >
            <nav class="navbar navbar-expand-lg pt-2 mt-0">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <a class="navbar-brand" href="{{route('home')}}" style="color: white">Inicio</a>
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('CajeroMenu.index')}}" style="color: white">Cajero</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('Venta.index')}}" style="color: white">Mesero</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('Taquero.index')}}" style="color: white">Taquero</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <div class="container-fluid pt-4 pr-5 pl-5">
        <div class="row">
            <div class="col-lg-6 col-12">
                <div class="row">
                    <did class = "col-12">
                        <h1 class="text-center">Nuevo Pedido</h1>
                    </did>

                </div>
                <div class="row">
                    @foreach($Articulos as $Articulo)
                        <div class="col-12 col-lg-4 col-md-6 col-sm-6 mt-4 mb-3">
                            <div class="card" >

                                <img src="{{URL::asset('Imagenes/'.$Articulo->Imagen)}}" class="card-img-top" alt="..." style="height: 200px">
                                <div class="card-body" style="background: teal; color: white">

                                        <p class="card-text"><h3>{{$Articulo->Nombre}}</h3><br>
                                        <h5 name="Precio">$ {{$Articulo->Precio}} Pesos</h5> <br>
                                        {{$Articulo->Descripcion}} <br>

                                        </p>
                                    <form action="{{route('DetallesVenta.store')}}" method="POST">
                                        @csrf
                                        <div class="d-none">
                                            <input class="d-none" type="text" name="Id" value="{{$Articulo->IdPlatillo}}">
                                            <input class="d-none" type="text" name="Precio" value="{{$Articulo->Precio}}">
                                        </div>
                                        <button type="submit" class="btn btn-primary añadir">Añadir</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>

            <div class="col-lg-6 col-12">
                <table class="table">
                    <thead style="background: darkorange; color: white">
                    <tr>
                        <th scope="col">Producto</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Precio Unitario</th>
                        <th scope="col">SubTotal</th>
                        <th scope="col">Comentarios</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($Detalles as $Detalle)

                    <tr>
                        <th>
                            {{$Detalle->Nombre}}
                        </th>
                        <td>
                            <a class="mas" href="{{url('/Actualizar/'.$Detalle->FkIdPlatillo)}}"> ▲     </a>
                            <a class="boton" data-toggle="modal" data-target="#exampleModal{{$Detalle->id}}">{{$Detalle->Cantidad}} </a>

                            <!--Modal para actualizar la cantidad mas rapido-->

                            <div class="modal fade" id="exampleModal{{$Detalle->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header" style="background: darkorange; color: white">
                                            <h5 class="modal-title" id="exampleModalLabel">Agregar más producto</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('Cantidad',$Detalle->id)}}" method="POST">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="Cantidad">Cantidad de {{$Detalle->Nombre}}</label>
                                                    <input type="text" class="form-control" id="Cantidad" name="Cantidad" value="{{$Detalle->Cantidad}}">
                                                </div>
                                                <button type="submit" class="btn btn-primary">Guardar</button>

                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Fin Modal para actualizar la cantidad--->



                            @if($Detalle->Cantidad > 1)
                                <a class="mas" href="{{url('/Actualizarmenos/'.$Detalle->FkIdPlatillo)}}">       ▼ </a>
                                @endif
                        </td>
                        <td>${{$Detalle->Precio}}</td>
                        <td>${{$Detalle->Precio * $Detalle->Cantidad}}</td>
                        <td>

                                <div class="row">

                                        <form action="{{route('DetallesVenta.update',$Detalle->id)}}" method="POST">
                                            {{method_field('patch')}}
                                            @csrf
                                                <input class="form-control" type="text" name="Comentario" value="{{$Detalle->Comentarios}}">
                                                <button type="submit" class="mas btn" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                                        <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                                                    </svg>
                                                </button>
                                            <a href="{{url('/Comentario/'.$Detalle->id)}}" class="basura">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                                </svg>
                                            </a>
                                        </form>


                                </div>

                        </td>
                        <td>
                            <form action="{{url('/DetallesVenta/'.$Detalle->id)}}" method="POST">
                                {{method_field('DELETE')}}
                                @csrf

                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>
                        </td>
                    </tr>

                    @endforeach
                    </tbody>

                </table>
                <div class="row">
                    <div class="col-6 offset-6">
                        <h4>Su Total es: $ {{$Total}}</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <form action="{{route('Venta.update',$IdVenta)}}" method="POST">
                            {{method_field('patch')}}
                            @csrf
                            <div class="d-none">

                                <input class="d-none" type="text" name="Total" value="{{$Total}}">
                            </div>
                            <button type="submit" class="btn btn-primary">ConfirmarPedido</button>

                        </form>

                    </div>
                    <div class="col-6">
                        <a href="{{url('/Cancelar/'.$IdVenta)}}" class="btn btn-danger">Cancelar Pedido</a>
                    </div>
                </div>

            </div>

        </div>
    </div>




@endsection
