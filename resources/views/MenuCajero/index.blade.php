@extends('layouts.app')

@section('content')
    <div style="background: teal;">
        <div class="container"  >
            <nav class="navbar navbar-expand-lg pt-2 mt-0">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <a class="navbar-brand" href="{{route('home')}}" style="color: white">Inicio</a>
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('CajeroMenu.index')}}" style="color: white">Cajero</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('Venta.index')}}" style="color: white">Mesero</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('Taquero.index')}}" style="color: white">Taquero</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    </div>

    <div class="container text-center pt-4">
        <div class="row">
            <did class = "col-10">
                <h1>Menu </h1>
            </did>
            <did class="col-2">
                <a href="{{route('Venta.index')}}" class="btn" style="background: teal; color: white">Nuevo Pedido</a>
            </did>
        </div>
        <div class="row">
        @foreach($Articulos as $Articulo)
            <div class="col-12 col-lg-3 col-md-6 col-sm-6 mt-4 mb-3">
                <div class="card" >
                    <img src="Imagenes/{{$Articulo->Imagen}}" class="card-img-top" alt="..." style="height: 200px">
                    <div class="card-body" style="background: teal; color: white">
                        <p class="card-text"><h3>{{$Articulo->Nombre}}</h3><br>
                        <h5>$ {{$Articulo->Precio}} Pesos</h5> <br>
                        {{$Articulo->Descripcion}}
                        </p>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
@endsection
