@extends('layouts.app')

@section('content')

    <div style="background: teal;">
        <div class="container"  >
            <nav class="navbar navbar-expand-lg pt-2 mt-0">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <a class="navbar-brand" href="{{route('home')}}" style="color: white">Inicio</a>
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('CajeroMenu.index')}}" style="color: white">Cajero</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('Venta.index')}}" style="color: white">Mesero</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('Taquero.index')}}" style="color: white">Taquero</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <div class="container-fluid mt-2">
        <div class="row">
            @foreach($Ventas as $venta)
            <div class="col-6 col-lg-3 col-md-4 col-sm-6">


                    <div class="card">
                        <div class="card-header" style="background: darkorange; text-align: center; color: white" >
                            Pedido #{{$venta->id}} para {{$venta->Mesa}}
                        </div>
                        <div class="card-body">
                            <table class="table">
                                        <thead  style="background: darkorange; color: white">
                                        <tr>
                                            <th scope="col">Cantidad</th>
                                            <th scope="col">Producto</th>
                                            <th scope="col">Comentario</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @for($i=0;$i<count($Detalles);$i++)
                                            @if($venta->id == $Detalles[$i]->FkIdVenta)
                                        <tr>
                                            <th> {{$Detalles[$i]->Cantidad}} </th>
                                            <td> {{$Detalles[$i]->Nombre}} </td>
                                            <td> {{$Detalles[$i]->Comentarios}} </td>
                                        </tr>
                                            @endif
                                        @endfor
                                        </tbody>
                                    </table>

                            <a href="{{route('Venta.edit',$venta->id)}}" class="btn btn-primary">Pedido Finalizado</a>
                        </div>
                    </div>


            </div>
            @endforeach
        </div>
    </div>

<!--Modal-->


@endsection
