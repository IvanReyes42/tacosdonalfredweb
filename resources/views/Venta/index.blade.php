@extends('layouts.app')

@section('content')

    <div style="background: teal;">
        <div class="container"  >
            <nav class="navbar navbar-expand-lg pt-2 mt-0">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <a class="navbar-brand" href="{{route('home')}}" style="color: white">Inicio</a>
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('CajeroMenu.index')}}" style="color: white">Cajero</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('Venta.index')}}" style="color: white">Mesero</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('Taquero.index')}}" style="color: white">Taquero</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <div class="row pt-4">
        <div class="col-7 offset-5">
            <!-- Boton Modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Nuevo Pedido
            </button>
            <!-- Fin Boton Modal -->
        </div>
    </div>
    <div class="row">
        <div class="container  pt-4">
            <table class="table">
                <thead style="background: darkorange; color: white">
                <tr>
                    <th scope="col">Pedido #</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Estado de Su Pedido</th>
                    <th scope="col">Total $</th>
                    <th scope="col">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($Ventas as $Venta)

                <tr>

                    <th>{{$Venta->id}}</th>
                    <td>{{$Venta->Mesa}} </td>
                    <td>{{$Venta->Estatus}}</td>
                    <td>$ {{$Venta->Total}}</td>
                    <td>
                        <div class="row">
                            <form>
                                <div class="col">
                                @if($Venta->Estatus == "En Proceso")
                                    <a class="btn btn-primary" href="{{route('DetallesVenta.show',$Venta->id)}}">Levantar Pedido</a>
                                    @endif
                                </div>
                            </form>
                            @if($Usuario->Puesto == "Cajero" || $Usuario->Puesto == "Administrador" )
                            @if($Venta->Estatus == "En Proceso")
                            <a class="btn btn-danger" href="{{url('/Cancelar/'.$Venta->id)}}">Cancelar Pedido</a>
                            @endif
                            @if($Venta->Estatus == "Por Cobrar")
                                <form>
                                    <div class="col">
                                        <a class="btn btn-danger" href="{{route('Cobrar.show',$Venta->id)}}">Cobrar</a>
                                    </div>
                                </form>
                            @endif
                            @endif
                        </div>
                    </td>
                </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background: darkorange; color: white">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Pedido</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('Venta.store')}}" method="POST">
                    @csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="Mesa">Mesa</label>
                            <select id="Mesa" class="form-control" name ="Mesa">
                                <option>Cliente para Llevar</option>
                                @for($i = 1; $i <=15; $i++)
                                <option value="Mesa {{$i}}"> Mesa {{$i}}</option>
                                @endfor
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="Cliente">Nombre Cliente</label>
                            <input type="text" class="form-control" id="Cliente" name="Cliente">
                            <small id="emailHelp" class="form-text text-muted">Este dato se llena solamente cuando es pedido para llevar</small>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary añadir" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
<!-- Fin Modal-->
@endsection
