<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        html {
            min-height: 100%;
            position: relative;
        }
        body {
            margin: 0;
            margin-bottom: 40px;
        }
        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 40px;
            color: white;
        }
        .btn-primary, .btn-primary:hover,.btn-primary:active{
            background: teal;
            border-color: teal;
        }

        .btn-danger, .btn-danger:hover, .btn-danger:active {
            background: darkorange;
            border-color: darkorange;
        }
        .mas, .mas:hover, .mas:active{
            color: teal;
        }
        .añadir, .añadir:hover, .añadir:active{
            background: darkorange;
            border-color: darkorange;
        }
        .basura, .basura:hover, .basura:active{
            color: darkorange;
        }
        .titulo{
            background: #EB984E;
            color:#EB984E;
        }
        .boton:hover{
            cursor: pointer;
            color: teal;
            text-decoration: none;
        }
        .boton{
            color: black;
            text-decoration: none;
        }
        .salir:hover
        {
            background-color: orange;
        }
    </style>
    <link rel="shortcut icon" href="Imagenes/Logo.ico" />
</head>
<body style="background: #ffe0b2">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg shadow-sm" style="background: darkorange; color: white">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto" >
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Entrar') }}</a>
                            </li>

                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="background-color: darkorange;">
                                    <a class="dropdown-item salir" style="color: white" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Salir') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="pb-0">

            @yield('content')
        </main>
    </div>
    <footer id="footer" class="pb-4 pt-4" style="background: darkorange; color:white; text-align: center" >
        <div class="container">
            <h6>Restaurante de Tacos Don Alfred Ignacio López Rayon 814, Centro, 47400 Lagos de Moreno, Jal.  </h6>
        </div>
    </footer>
    </body>
</html>
