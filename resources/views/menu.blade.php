@extends('layouts.app')

@section('menu')
<div style="background: teal;">
    <div class="container"  >
        <nav class="navbar navbar-expand-lg pt-2 mt-0">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="{{route('home')}}" style="color: white">Inicio</a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('CajeroMenu.index')}}" style="color: white">Cajero</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('Venta.index')}}" style="color: white">Mesero</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('Taquero.index')}}" style="color: white">Taquero</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
@endsection
