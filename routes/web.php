<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/CajeroMenu','MenuController@index')->name('MenuCajero');
Route::resource('CajeroMenu','MenuController');
Route::resource('Venta','VentaController');
Route::resource('DetallesVenta','DetallesVentaController');
Route::resource('Taquero','TaqueroController');
Route::resource('Cobrar','CobrarController');
Route::get('/Actualizar/{DetallesVentum}', 'DetallesVentaController@Actualizar')->name('Actualizar');
Route::get('/Comentario/{DetallesVentum}', 'DetallesVentaController@Comentario')->name('Comentario');
Route::get('/Cancelar/{Ventum}', 'VentaController@Cancelar')->name('Cancelar');
Route::get('/Actualizarmenos/{DetallesVentum}', 'DetallesVentaController@Actualizarmenos')->name('Actualizarmenos');
Route::get('/PDF/{Ventum}', 'CobrarController@pdf')->name('PDF');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/Cantidad/{DetallesVentum}', 'DetallesVentaController@Cantidad')->name('Cantidad');
